package de.artismarti.burndowntracker.persistence;

import java.util.ArrayList;
import java.util.List;

import de.artismarti.burndowntracker.logic.BurnDownEntry;
import de.artismarti.burndowntracker.logic.BurnDownTracker;
import de.artismarti.burndowntracker.logic.BurnDownTrackerBuilder;
import de.artismarti.burndowntracker.userinterface.listeners.OnTrackingChanged;

public class TrackingManager {

    private static final BurnDownTrackerBuilder builder = new BurnDownTrackerBuilder();

    static TrackingManager instance;

    private List<BurnDownTracker> trackers;
    private BurnDownTracker currentTracker;

    private List<OnTrackingChanged> subscribers;

    public static TrackingManager getInstance() {
        if (instance == null) {
            instance = new TrackingManager();
        }
        return instance;
    }

    private TrackingManager() {
        trackers = new ArrayList<>();
        subscribers = new ArrayList<>();
    }

    public void subscribeToCurrentTrackingChanges(OnTrackingChanged subscriber) {
        subscribers.add(subscriber);
    }

    public void loadTracker(BurnDownTracker tracker) {
        loadTrackerAndNotifyAndAddToMemory(tracker, true, true);
    }

    public void loadTrackerAndNotify(BurnDownTracker tracker, boolean notify) {
        loadTrackerAndNotifyAndAddToMemory(tracker, notify, false);
    }

    public void loadTrackerAndNotifyAndAddToMemory(BurnDownTracker tracker, boolean notify, boolean memory) {
        currentTracker = tracker;
        if (memory) {
            trackers.add(tracker);
        }
        if (notify) {
            notifySubscribersTrackingChanged();
        }
    }

    public void storeNewTracking(String name, int minutes, boolean negativeAllowed) {
        if (!negativeAllowed && minutes < 0) {
            return;
        }
        loadTracker(builder.setMinutes(minutes)
                .setAllowNegative(negativeAllowed)
                .setName(name)
                .create());
        notifySubscribersTrackingChanged();
    }

    public void addNewEntryToCurrentTracker(int minutes, String name, String timestamp) {
        currentTracker.addNewEntry(new BurnDownEntry(minutes, name, timestamp));
        notifySubscribersEntryAdded();
    }

    private void notifySubscribersEntryAdded() {
        for (OnTrackingChanged subscriber : subscribers) {
            subscriber.onNewEntryAdded();
        }
    }

    private void notifySubscribersTrackingChanged() {
        for (OnTrackingChanged subscriber : subscribers) {
            subscriber.onEntrySetChanged();
        }
    }

    public List<BurnDownTracker> getTrackers() {
        return trackers;
    }

    public List<BurnDownEntry> getCurrentTrackerEntries() {
        return currentTracker.getEntries();
    }

    public BurnDownTracker getCurrentTracker() {
        return currentTracker;
    }

    public boolean loadTrackerFromMemoryIfPresent(String trackerName) {
        if (currentTracker.getName().equals(trackerName)) {
            return true;
        }
        for (BurnDownTracker tracker : trackers) {
            if (tracker.getName().equals(trackerName)) {
                loadTrackerAndNotify(tracker, true);
                return true;
            }
        }
        return false;
    }
}
