package de.artismarti.burndowntracker.persistence;

import com.google.gson.Gson;

import de.artismarti.burndowntracker.logic.BurnDownTracker;

public class GsonProvider {

    private static GsonProvider instance;

    private static Gson gson;

    public static GsonProvider getInstance() {
        if (instance == null) {
            instance = new GsonProvider();
        }
        return instance;
    }

    private GsonProvider() {
        gson = new Gson();
    }

    public String writeTrackerToJson(BurnDownTracker tracker) {
        return gson.toJson(tracker);
    }

    public BurnDownTracker readTrackerFromJson(String referenceJsonTracker) {
        return gson.fromJson(referenceJsonTracker, BurnDownTracker.class);
    }

    public <T> String toJson(T toJson) {
        return gson.toJson(toJson);
    }
}
