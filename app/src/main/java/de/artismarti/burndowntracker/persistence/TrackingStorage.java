package de.artismarti.burndowntracker.persistence;

import android.content.Context;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import de.artismarti.burndowntracker.logic.BurnDownTracker;

public class TrackingStorage {

    public static final int LOADING_CAPACITY = 1000;
    private final Context context;
    private final GsonProvider gsonProvider;
    private static TrackingStorage instance;

    private TrackingStorage(Context context) {
        gsonProvider = GsonProvider.getInstance();
        this.context = context;
    }

    public static TrackingStorage getInstance(Context context) {
        if (instance == null) {
            instance = new TrackingStorage(context.getApplicationContext());
        }
        return instance;
    }

    public void saveTracker(BurnDownTracker tracker) throws IOException {
        String fileName = tracker.getName();
        String fileData = gsonProvider.writeTrackerToJson(tracker);
        FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
        fos.write(fileData.getBytes());
        fos.close();
    }

    public BurnDownTracker loadTracker(String trackerName) throws IOException {
        FileInputStream fis = context.openFileInput(trackerName);
        BufferedReader bf = new BufferedReader(new InputStreamReader(fis));
        StringBuilder data = new StringBuilder(LOADING_CAPACITY);
        String line = bf.readLine();
        while (line != null) {
            data.append(line);
            line = bf.readLine();
        }
        bf.close();
        return gsonProvider.readTrackerFromJson(data.toString());
    }

    public String[] getSavedTrackerNames() {
        return context.fileList();
    }
}
