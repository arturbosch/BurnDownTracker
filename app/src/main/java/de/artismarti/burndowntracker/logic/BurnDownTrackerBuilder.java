package de.artismarti.burndowntracker.logic;

import java.util.ArrayList;
import java.util.List;

public class BurnDownTrackerBuilder {

    private int minutes;
    private boolean allowNegative;
    private List<BurnDownEntry> entries;
    private String name;

    public BurnDownTrackerBuilder() {
        reset();
    }

    public BurnDownTrackerBuilder setMinutes(int minutes) {
        this.minutes = minutes;
        return this;
    }

    public BurnDownTrackerBuilder setAllowNegative(boolean allowNegative) {
        this.allowNegative = allowNegative;
        return this;
    }

    public BurnDownTrackerBuilder setEntries(List<BurnDownEntry> entries) {
        this.entries = entries;
        return this;
    }

    public BurnDownTrackerBuilder setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Creates a {@link de.artismarti.burndowntracker.logic.BurnDownTracker}
     * instance and resets then the builder attributes to default.
     * @return a BurnDownTracker instance
     */
    public BurnDownTracker create() {
        BurnDownTracker tracker =
                new BurnDownTracker(name, minutes, allowNegative, new ArrayList<>(entries));
        reset();
        return tracker;
    }

    private void reset() {
        minutes = 0;
        allowNegative = false;
        entries = new ArrayList<>();
        name = "Untitled";
    }
}