package de.artismarti.burndowntracker.logic;

public class BurnDownEntry {

    private final String reason;
    private final int minutes;
    private final String date;

    public BurnDownEntry(int minutes, String reason, String date) {
        this.minutes = minutes;
        this.reason = reason;
        this.date = date;
    }

    public int getMinutes() {
        return minutes;
    }

    public String getDate() {
        return date;
    }

    public String getReason() {
        return reason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BurnDownEntry)) {
            return false;
        }
        BurnDownEntry entry = (BurnDownEntry) o;

        return minutes == entry.minutes &&
                !(reason != null ? !reason.equals(entry.reason) : entry.reason != null);
    }

    @Override
    public int hashCode() {
        int result = reason != null ? reason.hashCode() : 0;
        result = 31 * result + minutes;
        return result;
    }
}
