package de.artismarti.burndowntracker.logic;

import java.util.List;

public class TrackerTimeCalculator {

    public static final double MINUTES_PER_HOURS_AS_DOUBLE = 60;

    public static int initMinutesDelta(List<BurnDownEntry> entries) {
        int minutesDelta = 0;
        for (BurnDownEntry entry : entries) {
            minutesDelta += entry.getMinutes();
        }
        return minutesDelta;
    }

    public static int setMinutesToZeroIfNegativeElseNothing(int minutesDelta) {
        return minutesDelta < 0 ? 0 : minutesDelta;
    }

    public static double initHoursDelta(int minutes) {
        return calcHoursDeltaFromMinutes(minutes);
    }

    private static double calcHoursDeltaFromMinutes(int minutes) {
        return minutes / MINUTES_PER_HOURS_AS_DOUBLE;
    }

    public static void calcNewTimeDeltas(BurnDownTracker tracker, int minutesToAdd) {
        tracker.setMinutesDelta(tracker.getMinutesDelta() + minutesToAdd);
        setMinutesToZeroIfNoNegativesAllowed(tracker);
        calculateDeltaHours(tracker);
    }

    private static void setMinutesToZeroIfNoNegativesAllowed(BurnDownTracker tracker) {
        if (!tracker.isNegativeAllowed()) {
            tracker.setMinutesDelta(
                    setMinutesToZeroIfNegativeElseNothing(tracker.getMinutesDelta()));
        }
    }

    private static void calculateDeltaHours(BurnDownTracker tracker) {
        tracker.setHoursDelta(calcHoursDeltaFromMinutes(tracker.getMinutesDelta()));
    }
}
