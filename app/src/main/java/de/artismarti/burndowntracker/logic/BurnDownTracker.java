package de.artismarti.burndowntracker.logic;

import java.util.List;

public class BurnDownTracker {

    private String name;
    private int minutesDelta;
    private double hoursDelta;
    private boolean negativeAllowed;
    private List<BurnDownEntry> entries;

    public BurnDownTracker(String trackerName, int neededMinutesAtStart,
                           boolean allowNegative, List<BurnDownEntry> entries) {
        this.name = trackerName;
        this.negativeAllowed = allowNegative;
        this.entries = entries;
        this.minutesDelta = neededMinutesAtStart;
        this.minutesDelta += TrackerTimeCalculator.initMinutesDelta(entries);
        if (!allowNegative) {
            this.minutesDelta = TrackerTimeCalculator.setMinutesToZeroIfNegativeElseNothing(minutesDelta);
        }
        this.hoursDelta = TrackerTimeCalculator.initHoursDelta(minutesDelta);
    }

    public void addNewEntry(BurnDownEntry burndownEntry) {
        entries.add(burndownEntry);
        TrackerTimeCalculator.calcNewTimeDeltas(this, burndownEntry.getMinutes());
    }

    public int getMinutesDelta() {
        return minutesDelta;
    }

    public double getHoursDelta() {
        return hoursDelta;
    }

    public boolean isNegativeAllowed() {
        return negativeAllowed;
    }

    public String getName() {
        return name;
    }

    public List<BurnDownEntry> getEntries() {
        return entries;
    }

    void setMinutesDelta(int minutesDelta) {
        this.minutesDelta = minutesDelta;
    }

    void setHoursDelta(double hoursDelta) {
        this.hoursDelta = hoursDelta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BurnDownTracker)) return false;

        BurnDownTracker tracker = (BurnDownTracker) o;

        if (minutesDelta != tracker.minutesDelta) return false;
        if (Double.compare(tracker.hoursDelta, hoursDelta) != 0) return false;
        //noinspection SimplifiableIfStatement
        if (negativeAllowed != tracker.negativeAllowed) return false;
        return entries.equals(tracker.entries);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = minutesDelta;
        temp = Double.doubleToLongBits(hoursDelta);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (negativeAllowed ? 1 : 0);
        result = 31 * result + entries.hashCode();
        return result;
    }
}
