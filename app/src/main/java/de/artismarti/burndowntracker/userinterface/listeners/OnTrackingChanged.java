package de.artismarti.burndowntracker.userinterface.listeners;

public interface OnTrackingChanged {

    void onNewEntryAdded();

    void onEntrySetChanged();

}
