package de.artismarti.burndowntracker.userinterface;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import de.artismarti.burndowntracker.R;
import de.artismarti.burndowntracker.settings.SettingsManager;
import de.artismarti.burndowntracker.userinterface.adapter.BurnDownEntryAdapter;
import de.artismarti.burndowntracker.logic.BurnDownEntry;
import de.artismarti.burndowntracker.logic.BurnDownTracker;
import de.artismarti.burndowntracker.persistence.TrackingManager;
import de.artismarti.burndowntracker.persistence.TrackingStorage;
import de.artismarti.burndowntracker.userinterface.listeners.OnTrackingChanged;
import de.artismarti.burndowntracker.userinterface.widgets.DividerItemDecoration;

public class MainActivity extends AppCompatActivity implements OnTrackingChanged {

    private static final String TAG = MainActivity.class.getSimpleName();
    private LinearLayout mainContent;
    private BurnDownEntryAdapter entryAdapter;

    private TextView overviewName;
    private TextView overviewMinutes;
    private TextView overviewHours;

    private TrackingManager trackingManager;
    private TrackingStorage trackingStorage;
    private SettingsManager settingsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainContent = (LinearLayout) findViewById(R.id.main_content);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        trackingManager = TrackingManager.getInstance();
        settingsManager = SettingsManager.getInstance(this);
        trackingManager.subscribeToCurrentTrackingChanges(this);
        trackingStorage = TrackingStorage.getInstance(this);
        initContent();
        initTrackerOverview();
        initFab();
        initTracker();
    }

    private void initTracker() {
        loadTracking(settingsManager.getLastUsedTracker());
    }

    private void initFab() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TrackingDialogs.createNewEntryDialog(view.getContext(), mainContent);
            }
        });
    }

    private void initContent() {
        List<BurnDownEntry> entryList = trackingManager.getCurrentTrackerEntries();
        entryAdapter = new BurnDownEntryAdapter(this, entryList);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setAdapter(entryAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, null));
    }

    private void initTrackerOverview() {
        CardView mCardView = (CardView) findViewById(R.id.tracking_overview);
        overviewName = (TextView) mCardView.findViewById(R.id.overview_name);
        overviewMinutes = (TextView) mCardView.findViewById(R.id.overview_minutes);
        overviewHours = (TextView) mCardView.findViewById(R.id.overview_hours);
        updateTrackerOverview();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_new_entry) {
            TrackingDialogs.createNewEntryDialog(this, mainContent);
            return true;
        }

        if (id == R.id.action_new_tracking) {
            TrackingDialogs.createNewTrackingDialog(this, mainContent);
            return true;
        }

        if (id == R.id.action_save_tracking) {
            saveCurrentTracking();
            return true;
        }

        if (id == R.id.action_load_tracking) {
            final String[] items = trackingStorage.getSavedTrackerNames();
            new AlertDialog.Builder(this)
                    .setCustomTitle(getLayoutInflater().inflate(R.layout.dialog_title_view, null))
                    .setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            loadTracking(items[which]);
                        }
                    })
                    .create().show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadTracking(String trackerName) {
        if (!trackingManager.loadTrackerFromMemoryIfPresent(trackerName)) {
            try {
                BurnDownTracker loadedTracker = trackingStorage.loadTracker(trackerName);
                trackingManager.loadTracker(loadedTracker);
            } catch (IOException ex) {
                Log.e(TAG, "Error reading from file system.");
                Toast.makeText(this, "Entered tracking name doesn't exist!", Toast.LENGTH_LONG).show();
            }
        }

    }

    private void saveCurrentTracking() {
        try {
            trackingStorage.saveTracker(trackingManager.getCurrentTracker());
        } catch (IOException ex) {
            Log.e(TAG, "Error writing to file system.");
            Toast.makeText(this, "An error occurred while writing to your file system.",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onNewEntryAdded() {
        entryAdapter.notifyItemInserted(entryAdapter.getItemCount() - 1);
        updateTrackerOverview();
    }

    @Override
    public void onEntrySetChanged() {
        entryAdapter.setData(trackingManager.getCurrentTrackerEntries());
        updateTrackerOverview();
    }

    private void updateTrackerOverview() {
        BurnDownTracker currentTracker = trackingManager.getCurrentTracker();
        String nameString = "Tracking: " + currentTracker.getName();
        overviewName.setText(nameString);
        String minutesString = "Total of " + currentTracker.getMinutesDelta() + " minutes left.";
        overviewMinutes.setText(minutesString);
        String hoursString = "Total of ~ " +
                (Math.round(currentTracker.getHoursDelta() * 100d) / 100d) + " hours left.";
        overviewHours.setText(hoursString);
    }
}
