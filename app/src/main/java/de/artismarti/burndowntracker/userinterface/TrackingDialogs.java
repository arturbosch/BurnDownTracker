package de.artismarti.burndowntracker.userinterface;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.Date;

import de.artismarti.burndowntracker.R;
import de.artismarti.burndowntracker.persistence.TrackingManager;

public class TrackingDialogs {

    public static void createNewTrackingDialog(final Context context, final View content) {
        @SuppressLint("InflateParams") final LinearLayout dialogLayout =
                (LinearLayout) LayoutInflater.from(context).inflate(R.layout.new_tracking_tracker, null);
        final EditText editName = (EditText) dialogLayout.findViewById(R.id.tracking_name);
        final EditText editMinutes = (EditText) dialogLayout.findViewById(R.id.tracking_minutes);
        final CheckBox checkNegative = (CheckBox) dialogLayout.findViewById(R.id.tracking_negatives);

        new AlertDialog.Builder(context)
                .setView(dialogLayout)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (editName.getText().toString().trim().isEmpty()) {
                            dialog.cancel();
                            createNewTrackingErrorSnakebarWithReplyAction(context, content);
                        } else {
                            storeNewTracking(editMinutes, editName, checkNegative);
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Nothing to do here
                    }
                })
                .create()
                .show();
    }

    private static void createNewTrackingErrorSnakebarWithReplyAction(final Context context, final View view) {
        Snackbar.make(view, "Invalid name! Please try again.", Snackbar.LENGTH_LONG)
                .setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        createNewTrackingDialog(context, view);
                    }
                }).show();
    }

    private static void storeNewTracking(EditText editMinutes, EditText editName, CheckBox checkNegative) {
        int minutes = editMinutes.getText().toString().trim().isEmpty()
                ? 0
                : Integer.parseInt(editMinutes.getText().toString());
        TrackingManager.getInstance().storeNewTracking(
                editName.getText().toString(),
                minutes,
                checkNegative.isChecked());
    }

    public static void createNewEntryDialog(final Context context, final View content) {
        @SuppressLint("InflateParams") final LinearLayout dialogLayout =
                (LinearLayout) LayoutInflater.from(context).inflate(R.layout.new_tracking_entry, null);
        final EditText editName = (EditText) dialogLayout.findViewById(R.id.tracking_entry_name);
        final EditText editMinutes = (EditText) dialogLayout.findViewById(R.id.tracking_entry_minutes);
        final CheckBox checkSubtract = (CheckBox) dialogLayout.findViewById(R.id.tracking_entry_subtract);

        new AlertDialog.Builder(context)
                .setView(dialogLayout)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (editName.getText().toString().trim().isEmpty()
                                || editMinutes.getText().toString().trim().isEmpty()) {
                            dialog.cancel();
                            createNewTrackingEntryErrorSnakebarWithReplyAction(context, content);
                        } else {
                            String date = DateFormat.getLongDateFormat(context).format(new Date());
                            storeNewEntry(editMinutes, editName, checkSubtract, date);
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Nothing to do here
                    }
                })
                .create()
                .show();
    }

    private static void storeNewEntry(EditText editMinutes, EditText editName,
                                      CheckBox checkSubtract, String date) {
        int minutes = Integer.parseInt(editMinutes.getText().toString());
        if (checkSubtract.isChecked()) {
            minutes = minutes * -1;
        }
        TrackingManager.getInstance().addNewEntryToCurrentTracker(
                minutes, editName.getText().toString(), date);
    }

    private static void createNewTrackingEntryErrorSnakebarWithReplyAction(final Context context,
                                                                           final View content) {
        Snackbar.make(content, "Invalid inputs! Please try again.", Snackbar.LENGTH_LONG)
                .setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        createNewEntryDialog(context, content);
                    }
                }).show();
    }
}
