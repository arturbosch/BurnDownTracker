package de.artismarti.burndowntracker.userinterface.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.artismarti.burndowntracker.R;
import de.artismarti.burndowntracker.logic.BurnDownEntry;

public class BurnDownEntryAdapter extends RecyclerView.Adapter<BurnDownEntryAdapter.EntryViewHolder> {

    private final LayoutInflater layoutInflater;
    private List<BurnDownEntry> entryList;

    public BurnDownEntryAdapter(Context context, List<BurnDownEntry> entryList) {
        layoutInflater = LayoutInflater.from(context);
        this.entryList = entryList;
        notifyDataSetChanged();
    }

    public void setData(List<BurnDownEntry> currentTrackerEntries) {
        this.entryList = currentTrackerEntries;
        notifyDataSetChanged();
    }

    @Override
    public EntryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.tracking_entry, parent, false);
        return new EntryViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return entryList.size();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onBindViewHolder(EntryViewHolder holder, int position) {
        BurnDownEntry entry = entryList.get(position);
        int min = entry.getMinutes();
        char symbol = '-';
        Drawable icon = layoutInflater.getContext().getResources()
                .getDrawable(R.drawable.ic_remove_circle_black_48dp);
        if (min > 0) {
            symbol = '+';
            icon = layoutInflater.getContext().getResources()
                    .getDrawable(R.drawable.ic_add_circle_black_48dp);
        }
        String timeString = symbol + " " + Math.abs(min) + " min";
        holder.time.setText(timeString);
        holder.icon.setImageDrawable(icon);
        holder.date.setText(entry.getDate());
        holder.reason.setText(entry.getReason());
    }

    class EntryViewHolder extends RecyclerView.ViewHolder {

        private ImageView icon;
        private TextView reason;
        private TextView date;
        private TextView time;

        public EntryViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.entry_icon);
            reason = (TextView) itemView.findViewById(R.id.entry_reason);
            date = (TextView) itemView.findViewById(R.id.entry_date);
            time = (TextView) itemView.findViewById(R.id.entry_minutes);
        }
    }
}

