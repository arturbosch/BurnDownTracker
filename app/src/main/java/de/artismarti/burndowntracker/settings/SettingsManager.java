package de.artismarti.burndowntracker.settings;

import android.content.Context;

import java.io.FileOutputStream;
import java.io.IOException;

import de.artismarti.burndowntracker.persistence.GsonProvider;

/**
 * @author artur
 */
public class SettingsManager {

    private static final String SETTINGS_FILE = "SETTINGS_FILE.txt";

    private Settings settings;

    private final Context context;
    private static SettingsManager instance;
    private GsonProvider gsonProvider;

    private SettingsManager(Context context) {
        this.context = context;
        this.gsonProvider = GsonProvider.getInstance();
    }

    public String getLastUsedTracker() {
        return null;
    }

    public void saveSettings() {
        try {
            writeToFile(SETTINGS_FILE, gsonProvider.toJson(settings));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeToFile(String fileName, String fileData) throws IOException {
        FileOutputStream fos = context.openFileOutput(SETTINGS_FILE, Context.MODE_PRIVATE);
        fos.write(fileData.getBytes());
        fos.close();
    }

    public void loadSettings() {

    }

    public static SettingsManager getInstance(Context context) {
        if (instance == null) {
            instance = new SettingsManager(context);
        }
        return instance;
    }

}
