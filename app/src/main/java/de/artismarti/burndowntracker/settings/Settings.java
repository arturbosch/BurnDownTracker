package de.artismarti.burndowntracker.settings;

/**
 * @author artur
 */
class Settings {

    private String trackerName = "";
    private TimeMode timeMode = TimeMode.MODE_MINUTES;
    private Language language = Language.ENGLISH;

    public String getTrackerName() {
        return trackerName;
    }

    public void setTrackerName(String trackerName) {
        this.trackerName = trackerName;
    }

    public TimeMode getTimeMode() {
        return timeMode;
    }

    public void setTimeMode(TimeMode timeMode) {
        this.timeMode = timeMode;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}
