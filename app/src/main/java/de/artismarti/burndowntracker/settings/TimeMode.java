package de.artismarti.burndowntracker.settings;

/**
 * @author artur
 */
enum TimeMode {
    MODE_MINUTES,
    MODE_HOURS
}