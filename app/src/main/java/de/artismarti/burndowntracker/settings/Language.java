package de.artismarti.burndowntracker.settings;

/**
 * @author artur
 */
enum Language {
    GERMAN,
    ENGLISH
}
