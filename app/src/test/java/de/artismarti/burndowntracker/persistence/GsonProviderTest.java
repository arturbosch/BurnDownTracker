package de.artismarti.burndowntracker.persistence;

import org.junit.Before;
import org.junit.Test;

import de.artismarti.burndowntracker.logic.BurnDownTracker;
import de.artismarti.burndowntracker.logic.TrackerTestData;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class GsonProviderTest extends TrackerTestData {

    private static final String REFERENCE_JSON_TRACKER =
            "{\"name\":\"Untitled\",\"minutesDelta\":57,\"hoursDelta\":0.95,\"negativeAllowed\":false,\"entries\":[" +
                    "{\"reason\":\"TestCase\",\"minutes\":19,\"date\":\"25.10.15 00:00\"}," +
                    "{\"reason\":\"TestCase\",\"minutes\":19,\"date\":\"25.10.15 00:00\"}," +
                    "{\"reason\":\"TestCase\",\"minutes\":19,\"date\":\"25.10.15 00:00\"}]}";

    private GsonProvider gsonProvider;
    private BurnDownTracker tracker;

    @Before
    public void setUp() {
        super.setUp();
        gsonProvider = GsonProvider.getInstance();
        tracker = getTrackerWithThreeEntriesAndSetNegative(false);
    }

    @Test
    public void fromDataToGsonWithPositiveTracker() {
        String storedJsonString = gsonProvider.writeTrackerToJson(tracker);

        assertThat(storedJsonString, is(REFERENCE_JSON_TRACKER));
    }

    @Test
    public void fromDataToGsonWithNegativeTracker() {
        BurnDownTracker referenceTracker = gsonProvider.readTrackerFromJson(REFERENCE_JSON_TRACKER);

        assertThat(referenceTracker, is(tracker));
    }
}
