package de.artismarti.burndowntracker.persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.artismarti.burndowntracker.logic.TrackerTestData;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class TrackingManagerTest {

    public static final String NEW_TRACKER_NAME = "NewTracker";

    private TrackingManager trackingManager;

    @Before
    public void setUp() {
        trackingManager = TrackingManager.getInstance();
    }

    @After
    public void tearDown() {
        TrackingManager.instance = null;
    }

    @Test
    public void storeNewTrackingWithValidInputs() {
        trackingManager.storeNewTracking(NEW_TRACKER_NAME, 100, false);

        assertFalse(trackingManager.getTrackers().isEmpty());
    }

    @Test
    public void storeNewNegativeTrackingWithNegativeStartMinutesExpectNotEmptyList() {
        trackingManager.storeNewTracking(NEW_TRACKER_NAME, -100, true);

        assertFalse(trackingManager.getTrackers().isEmpty());
    }

    @Test
    public void storeNewPositiveTrackerWithNegativeMinutesExpectEmptyTrackerList() {
        trackingManager.storeNewTracking(NEW_TRACKER_NAME, -100, false);

        assertTrue(trackingManager.getTrackers().isEmpty());
    }

    @Test
    public void addNewEntryToCurrentTrackerExpectEntriesPlusOne() {
        trackingManager.storeNewTracking(NEW_TRACKER_NAME, 500, false);
        trackingManager.addNewEntryToCurrentTracker(100, "NEW_ENTRY_NAME", TrackerTestData.getTimeStamp());

        assertTrue(trackingManager.getCurrentTrackerEntries().size() > 0);
    }
}