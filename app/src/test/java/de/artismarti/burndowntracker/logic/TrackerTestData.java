package de.artismarti.burndowntracker.logic;

import android.support.annotation.NonNull;

import org.junit.Before;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;

public class TrackerTestData {

    protected static final int MINUTES_19 = 19;
    protected static final String NEW_ENTRY_STRING = "TestCase";
    protected static final int NEGATIVE_MINUTES_10 = -10;
    protected static final int START_TIME_20 = 20;
    protected static final double DOUBLE_19_DIV_60 = 0.33;
    protected static final double DIV_MINUS_20_60 = -0.33;
    protected static final double ERROR = 0.05;
    protected static final double DIV_28_60 = 0.45;

    private BurnDownTrackerBuilder builder;

    @Before
    public void setUp() {
        builder = new BurnDownTrackerBuilder();
    }

    @NonNull
    protected BurnDownTracker getNegativeTracker() {
        return builder.setAllowNegative(true).create();
    }

    @NonNull
    protected BurnDownTracker getPositiveTracker() {
        return builder.setAllowNegative(false).create();
    }

    @NonNull
    protected BurnDownTracker getTrackerWithThreeEntriesAndSetNegative(boolean isNegative) {
        return builder
                .setAllowNegative(isNegative)
                .setEntries(Arrays.asList(
                        getPositiveBurnDownEntry(),
                        getPositiveBurnDownEntry(),
                        getPositiveBurnDownEntry()))
                .create();
    }

    @NonNull
    protected BurnDownEntry getPositiveBurnDownEntry() {
        return new BurnDownEntry(MINUTES_19, NEW_ENTRY_STRING, getTimeStamp());
    }

    public static String getTimeStamp() {
        return new SimpleDateFormat().format(new Date(new GregorianCalendar(2015, 9, 25).getTimeInMillis()));
    }

    @NonNull
    protected BurnDownEntry getNegativeBurnDownEntry() {
        return new BurnDownEntry(NEGATIVE_MINUTES_10, NEW_ENTRY_STRING, getTimeStamp());
    }
}
