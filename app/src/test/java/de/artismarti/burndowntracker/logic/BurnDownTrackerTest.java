package de.artismarti.burndowntracker.logic;

import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.IsCloseTo.closeTo;

public class BurnDownTrackerTest extends TrackerTestData {

    @Test
    public void addNewEntryCheckDeltaTime() {
        BurnDownTracker tracker = getPositiveTracker();
        tracker.addNewEntry(getPositiveBurnDownEntry());

        assertThat(tracker.getMinutesDelta(), is(MINUTES_19));
        assertThat(tracker.getHoursDelta(), closeTo(DOUBLE_19_DIV_60, ERROR));
    }

    @Test
    public void allowNegativeNumbers() {
        BurnDownTracker tracker = getNegativeTracker();
        tracker.addNewEntry(getNegativeBurnDownEntry());
        tracker.addNewEntry(getNegativeBurnDownEntry());

        assertThat(tracker.getMinutesDelta(), is(NEGATIVE_MINUTES_10 + NEGATIVE_MINUTES_10));
        assertThat(tracker.getHoursDelta(), closeTo(DIV_MINUS_20_60, ERROR));
    }

    @Test
    public void disallowNegativeNumbers() {
        BurnDownTracker tracker = getPositiveTracker();
        tracker.addNewEntry(getNegativeBurnDownEntry());
        tracker.addNewEntry(getNegativeBurnDownEntry());

        assertThat(tracker.getMinutesDelta(), is(0));
    }

    @Test
    public void initTrackerWithEntriesAndExpectDeltaTime() {
        BurnDownTracker tracker = new BurnDownTrackerBuilder().setEntries(
                Arrays.asList(getPositiveBurnDownEntry(),
                        getNegativeBurnDownEntry(),
                        getPositiveBurnDownEntry())).create();

        assertThat(tracker.getMinutesDelta(), is(MINUTES_19 + MINUTES_19 + NEGATIVE_MINUTES_10));
        assertThat(tracker.getHoursDelta(), closeTo(DIV_28_60, ERROR));
    }

    @Test
    public void initTrackerWithStartTimeAndEntries() {
        BurnDownTracker tracker =
                new BurnDownTrackerBuilder()
                        .setAllowNegative(true)
                        .setMinutes(START_TIME_20)
                        .setEntries(Arrays.asList(
                                getPositiveBurnDownEntry(),
                                getNegativeBurnDownEntry(),
                                getNegativeBurnDownEntry(),
                                getNegativeBurnDownEntry(),
                                getNegativeBurnDownEntry()))
                        .create();

        assertThat(tracker.getMinutesDelta(), is(-1));
    }
}